<?php

session_start();
if(!$_SESSION['userName']){
	header("Location: index.php");
}

require_once '../includes/header.html';

?>

	<div class="header">
		<h1>Admin List</h1>
	</div>
	<br />
	
	<div id="adminList">
		<table class="pure-table">
			<thead>
			    <tr>
				    <th>Admin ID</th>
					<th>User Name</th>
				</tr>
			</thead>
			<tbody>
			
<?php
		
	require_once "../includes/db_connection.php";
	$query = "SELECT * FROM transit.secure_login WHERE (1=1);";
	
	$run = mysqli_query($dbcon,$query);
	
	while($row = mysqli_fetch_assoc($run)){
				$userID = $row['USER_ID'];
				$userName = $row['USERNAME'];
				echo "<tr><td>$userID</td><td>$userName</td></tr>\n";
	}
	
		
?>
			</tbody>
		</table>
		</div>
		
		<div id = "editing">
			<h1 class= "header">Add or Edit Administrators</h1><br/>
			<div id="theButtons">
			<a id = "addButton" type="button" class="button-success pure-button" href="addAdmin.php">Add New Administrator</a><br /><br />
			<a id="editButton" type="button" class="button-success pure-button" href="editAdmin.php">Edit Administrator Information</a>
			</div>
		</div>
	
	
	</div>
	
<?php require_once '../includes/footer.html' ?>