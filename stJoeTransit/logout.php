<?php
session_start(); //Start the session to load the cookie.
session_destroy();  //Destroy the cookie.

header("Location: index.php"); //Load back to login prompt.