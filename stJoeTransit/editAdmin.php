<?php 
session_start();

if(!$_SESSION['userName']){
	header("Location: index.php");
}

require_once '../includes/header.html'; 
require_once '../includes/db_connection.php'; ?>

    <div class="header">
	    <h1>Edit an Administrator's Information</h1>
		<h2 class="content-subhead">Select an Admin to Edit</h2><br />
	</div>
	
	<br /><div id="editAdmins">
	    <form class = "pure-form" id="selectAdmin" action="editAdmin.php" name="editAdminsForm" method="POST">
		<select id="selectOne" name="selectOne">
			<?php
			
			$query = "SELECT * FROM secure_login;";
			$run = mysqli_query($dbcon,$query);
			while($row = mysqli_fetch_array($run)){
				$userID = $row[0];
				$username = $row[1];
				$password = $row[2];
				echo "<option value=\"$userID\">[$userID] $username</option>\n\t\t\t";
			}
			
			?>
			
		</select><br /><br />
		<button type="submit" name="submit" class="pure-button pure-button-primary">Edit</button>
		</form>	<br /> <br />
		
		<?php
		
		if(isset($_POST['submit']) && !isset($_POST['submitTheEdit'])){
			$userID = $_POST['selectOne'];
			$query = "SELECT * FROM `secure_login` WHERE (1=1) AND USER_ID = $userID";
			
			$run = mysqli_query($dbcon,$query);
			$row = mysqli_fetch_array($run);
			
			$username = $row[1];
			$password = $row[2];
			echo "<form class='pure-form' id='editAdmin' name='editAdmin' action='editAdmin2.php' method='POST'>";
			echo "Username: <input type='text' placeholder='$username' name='userEdit2'><br /><br />";
			echo "Password: <input type='text' placeholder='$password' name='passEdit2'><br /><br />";
			echo "<input type='hidden' name='idEdit2' value='$userID'>";
			echo "<button type='submit' name='submitTheEdit' class='pure-button pure-button-primary'>Finish the Edit</button>";
			echo "</form>";			
		}		
		?>
	</div>

<?php require_once '../includes/footer.html'; ?>