<?php
session_start();

if(!$_SESSION['userName']){
	header("Location: index.php");
}

require_once '../includes/header.html';
require_once '../includes/db_connection.php';
?>

    <div class="header">
	    <h1>Add Administrator Account</h1>
        <h2 class="content-subhead">Include the Information Below</h2>
	</div><br />
	
	<div id = "addAdminForm">
	    <form class="pure-form" method="POST" action="addAdmin.php" id="addAdmin">
		    Username: <input type="text" id="addUser" name="addUser" placeholder="Username"/><br /><br />
			Password:  <input type="password" id="addPass" name="addPass" placeholder="Password"/><br /><br />
			<button type="submit" class="pure-button pure-button-primary" name="submitNewUser">Create User</button>
		</form>
		
		<script type="text/javascript">
	        var frmvalidator  = new Validator("addAdmin");
	        frmvalidator.addValidation("addUser","req","Please Enter a Username");
	        frmvalidator.addValidation("addPass","req","Please Enter a Password");
        </script>
	</div>
		
<?php 



	
if(isset($_POST['submitNewUser'])){
	//convert HTML string into a PHP string, gets rid of malicious input.
	$user_name = htmlentities($_POST['addUser']);
	$pass_word = htmlentities($_POST['addPass']);
	
	
	//Find the current number of users on the Admin List and 
	//add one to make the new users' ID.
	$query = "SELECT COUNT(*) FROM `secure_login` WHERE (1=1);";
	$run = mysqli_query($dbcon,$query);
	$row = mysqli_fetch_array($run);
	$idNumber = $row[0] + 1;

	//Insert the new User and Pass into the database.
	$queryFinal = "INSERT INTO secure_login VALUES ('$idNumber','$user_name','$pass_word');";
	$row = mysqli_query($dbcon,$queryFinal);
	echo "<script>window.open('userAdmins.php','_self')</script>";
	
}

?>
		
<?php require_once '../includes/footer.html'; ?>