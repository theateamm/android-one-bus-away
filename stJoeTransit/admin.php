

<?php 
session_start();
if(!$_SESSION['userName']){
	header("Location: index.php");
}

require_once '../includes/header.html'; 
?>

		<div class="header">
			<h1>Transit App Admin Portal</h1>
			<h2 class="content-subhead">Select an Option Below</h2><br />
		</div><br />

		
		<div id="theButtons">
			<a id="button1" type="button" class="pure-button pure-button-primary" href="passenger.php">Passenger Data</a><br /><br />
			<a id="button2" type="button" class="pure-button pure-button-primary" href="routes.php">Routes</a><br /><br />
			<a id="button3" type="button" class="pure-button pure-button-primary" href="userAdmins.php">Admin List</a><br /><br />
			<a id="button4" type="button" class="pure-button pure-button-primary" href="buses.php">Bus List</a>
		</div>



<?php require_once '../includes/footer.html'; ?>