<?php 
//ALWAYS start a session on every page to check for the cookie.  The succeesive line will check if they are actually an admin on this site.
session_start();


if(isset($_SESSION['userName'])){
	//automatically send the user to the admin portal if their login information is still in the set cookie.  Else, will just prompt them to login.
	echo "<script>window.open('admin.php','_self')</script>";
}

require_once '../includes/header.html'; 
?>

		<div class="header">
			<h1>Transit App Admin Portal</h1>
			<h2>Please Sign In:</h1>
		</div>
		
		<div class="content">
		<div style="display:table; margin: 0 auto;"><br />
			<form id="loginForm" class="pure-form" action="index.php" method="POST" name="loginForm">
				<input type="text" name="userName" autocomplete="off" placeholder="Username"><br />
				<input type="password" name="password" autocomplete="off" placeholder="Password"><br /><br />
				<button style="display:table; margin:0 auto;" type="submit" name="submit" class="pure-button pure-button-primary">Sign In</button>
		
			<script type="text/javascript">
				var frmvalidator = new Validator("loginForm");
				frmvalidator.addValidation("userName","req","Please enter your Username");
				frmvalidator.addValidation("password","req","Please enter your Password");
			</script>
			
<?php
	//include the database connection.
	require_once "../includes/db_connection.php";
	
	if(isset($_POST['submit'])){
		
		//include the login check php file only if the user has entered a username and password combination.
		require_once "../includes/logincheck.php";
		
	}
?>
			
		</div>
		</div>
<?php require_once '../includes/footer.html'; ?>