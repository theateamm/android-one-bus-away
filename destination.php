<!DOCTYPE html>
<html>
<head>
	<!-- JQuery CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- End of JQuery CDN -->
	
	<!-- Start of Bootstrap CDN -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<!-- End of Boostrap CDN -->
	
	<!-- Start of Google Maps API -->
	<script src="https://maps.googleapis.com/maps-api-v3/api/js/22/7/main.js"></script>
	<style type="text/css">
      html, body { height: 100%;}
      #map { height: 75%; margin: 0 auto;}
	  #query {display: table; margin: 0 auto;}
	  .btn{width:12em; height:2em; color: white;}
	  .btn-primary{color:white;}
	  #buttonz{display: table; margin: 0 auto;}
    </style>
	<!-- End of Google Maps API -->
	
	<!-- Custom JS file for changing the DOM -->
	<script src="change.js"></script>
	<link rel="icon" href="favicon.ico">
	<title>St. Joseph Transit</title>
</head>

<body>
	<div id="map"></div>
	
	<div id="buttonz">
		<button class="btn btn-primary" name ="submit" value="Submit" onclick="window.location.href = '/onebusaway/index.php'">Try Another</button>
	</div>
	
	<script type="text/javascript" class="mapScript" id="theScript">
	var map;
	var initialLocation;
	var marker;
	var browserSupportFlag = new Boolean();
	var myLatLng;
		function initMap() {
				myLatLng = {lat: 39.758344, lng: -94.784935};
		
				map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: 39.7570484, lng: -94.787038},
				zoom: 15,
				});
			
				var transitLayer = new google.maps.TransitLayer();
				transitLayer.setMap(map);
				
				//W3C Geolocation
				if(navigator.geolocation){
					browserSupportFlag = true;
					navigator.geolocation.getCurrentPosition(function(position){
						initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
						map.setCenter(initialLocation);
						marker = new google.maps.Marker({
							position: initialLocation,
							map: map,
							title: 'Your Determined Location'
						});
					}, function(){
						handleNoGeolocation(browserSupportFlag);
					});
				}
				//Browser doesn't support Geolocation
				else{
					browserSupportFlag = false;
					handleNoGeolocation(browserSupportFlag);
				}
				
				function handleNoGeolocation(errorFlag){
					if(errorFlag == true){
						alert("Geolocation service failed. We've centered you on MWSU.");
						initialLocation = myLatLng;
						marker = new google.maps.Marker({
							position: myLatLng,
							map: map,
							title: 'Missouri Western State University'
						});
					}else{
						alert("Your browser doesn't support geolocation.  We've centered you on MWSU.");
						initialLocation = myLatLng;
						marker = new google.maps.Marker({
							position: myLatLng,
							map: map,
							title: 'Missouri Western State University'
						});
					}
					map.setCenter(intialLocation);
				}
				
		}
    </script>
    <script class = "mapScript" id="theLink" async defer
      src="https://maps.googleapis.com/maps/api/directions/json?origin=Missouri+Western+State+University&destination=South+Belt+Walmart+Saint+Joseph+Missouri&key=AIzaSyBClC5QbctDp55bcTOhjDdbEhbOOs5DJPE">
    </script>
</body>
</html>